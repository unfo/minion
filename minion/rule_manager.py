from typing import Optional, Dict

from minion import condition
from minion.interfaces import DataFile, Rule, FileManager, RuleLibrary


class RuleConditionValues(condition.ConditionValues):
    def __init__(self, files: 'RuleManager', root: DataFile):
        self.files = files
        self.root = root
        self.known_values: Dict[str, str] = {}

    def get(self, value_name: str, default_value: str = None) -> Optional[str]:
        if value_name in self.known_values:
            return self.known_values[value_name]
        value = self.files.get_string(self.root, value_name)
        if value is None:
            return default_value
        value = value.strip()
        if not value:
            return default_value
        used_value = value[0:1024]  # limit the data
        return used_value


class RuleManager:
    def __init__(self, files: FileManager):
        self.files = files
        self.registry = self.files.registry
        self.rules = self.registry.rules

    def test_rule(self, file: DataFile, rule: Rule) -> bool:
        if not rule.predicate:
            return False
        if file.is_dir():
            return False  # NOTE: perhaps we allow directory-sourced rules later
        values = RuleConditionValues(self, file)
        values.known_values['name'] = file.path.name
        ch = rule.predicate.evaluate(values)
        return ch is not None and ch != ''

    def get_value(self, file: DataFile, value_name: str) -> Optional[DataFile]:
        value_f = self.files.get_target(file, value_name)
        if not value_f:
            # target not resolved, try to apply the rule
            all_rules = self.files.registry.resolve_rules(file)
            t_rule = all_rules.get(value_name) if all_rules else None
            if t_rule:
                value_f = self.files.apply_rule(file, t_rule)  # no callback, so wait for result
        return value_f

    def get_string(self, file: DataFile, value_name: str, default_value: Optional[str] = None) -> Optional[str]:
        value_f = self.get_value(file, value_name)
        if value_f is None:
            return default_value
        return value_f.read_string(default_value)