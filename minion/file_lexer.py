import logging
import pathlib

from lark import Lark, Transformer, v_args
from typing import List

import minion.interfaces
from minion import condition, interfaces, rules

action_grammar = """
    start: (keep | import | rule)*
    keep: "keep" ( keep_all | keep_rule (", " keep_rule)* )
    keep_all: "all" -> keep_all
    keep_rule: NAME -> keep_rule
    import: "import" STRING -> import_file
    rule: "rule" rule_name [options] [when] actions -> rule
    rule_name: NAME -> rule_name
    when: "all" -> all
      | "when" cond -> condition
    actions: (command)*
    options: "batch" -> option_batch
    command: COMMAND -> command

    cond: operand              -> operand
          | cond "and" operand -> and_cond
          | cond "or" operand  -> or_cond
          | "not" cond         -> not_cond
          | "(" cond ")"       -> grouped_cond

    operand: op_value "==" op_value  -> eq_cond
          | op_value "!=" op_value   -> not_eq_cond
          | op_value "//" op_value  -> string_match
          | op_value                 -> op_value

    op_value: STRING -> reference

    WS.1: /[ \\t\\r\\n]+/
    %ignore WS

    COMMENT.1: /[ \\t]*#.*/
    %ignore COMMENT

    %import common.CNAME -> NAME

    %import common.ESCAPED_STRING -> ESCAPED_STRING
    PLAIN_STRING: /[^\\" '\\\\\\n\\t\\r]+/
    STRING: ESCAPED_STRING | PLAIN_STRING

    COMMAND.2: /\\n[ \t]+[\\S][^\\n]*/
"""

@v_args(inline=True)  # Affects the signatures of the methods
class RuleParsingTree(Transformer):
    def __init__(self):
        super().__init__()
        self.rule_cursor = rules.CommandRule()
        self.rule_file = minion.interfaces.RuleFile([])
        self.logger = logging.getLogger("parser")

    def keep_all(self):
        self.rule_file.extend.add("*")
        return None

    def keep_rule(self, name):
        self.rule_file.extend.add(name.value)
        return None

    def rule_name(self, name):
        self.rule_cursor.name = name.value
        return None

    def all(self):
        self.rule_cursor.predicate = condition.ConditionReference("'all'")
        return None

    def condition(self, cond):
        self.rule_cursor.predicate = cond
        return None

    def and_cond(self, left, right):
        return condition.AndCondition(left, right)

    def or_cond(self, left, right):
        return condition.OrCondition(left, right)

    def not_cond(self, sub):
        return condition.NotCondition(sub)

    def operand(self, sub):
        return sub

    def grouped_cond(self, sub):
        return sub

    def op_value(self, sub):
        return sub

    def reference(self, value):
        return condition.ConditionReference(value.value)

    def eq_cond(self, left, right):
        return condition.EqualCondition(left, right)

    def not_eq_cond(self, left, right):
        return condition.NotCondition(condition.EqualCondition(left, right))

    def string_match(self, sub, pattern):
        return condition.ConditionSimpleMatch(sub, pattern)

    def option_batch(self):
        self.rule_cursor.batch = True
        return None

    def command(self, line):
        self.rule_cursor.commands.append(line.value.strip())
        return None

    def rule(self, *args):
        self.rule_file.rules = list(filter(lambda r: r.name != self.rule_cursor.name, self.rule_file.rules))
        self.rule_file.rules.append(self.rule_cursor)
        self.rule_file.update_maps()
        self.rule_cursor = rules.CommandRule()
        return None

    def import_file(self, file):
        i_file = pathlib.Path(file.strip())
        if not i_file.is_file():
            i_file = (pathlib.Path(__file__).parent / "builtin" / "rules" / i_file).resolve()
        self.logger.debug(f"import {i_file.as_posix()}")
        with i_file.open("rt") as f:
            ir = parse(f.read())
            for r in ir.rules:
                self.rule_file.rules.append(r)
        return None


def parse(text) -> minion.interfaces.RuleFile:
    result = RuleParsingTree()
    file_parser = Lark(action_grammar, parser='lalr', transformer=result)
    file_parser.parse(text)
    return result.rule_file
