import json
import pathlib
from typing import Optional, Iterable, Dict, List

from minion.interfaces import DataFile, Rule, FileMetadata, FileRegistry, RuleLibrary, FileManager, RuleFile


class SimpleRuleLibrary(RuleLibrary):
    def __init__(self, rules: List[Rule] = None):
        self.rules = RuleFile(rules or [])

    def get_rule_file(self) -> Optional[RuleFile]:
        return self.rules


class DefaultFileRegistry(FileRegistry):
    def __init__(self, root_dir: pathlib.Path, rules: RuleLibrary = SimpleRuleLibrary()):
        super(DefaultFileRegistry, self).__init__(rules)
        self.root_dir = root_dir
        self.root_dir = self.root_dir.resolve()
        self.meta_root_dir = self.root_dir / ".minion" / "meta"
        self.meta_root_dir.mkdir(parents=True, exist_ok=True)

    def get_file_by_path(self, path: str) -> Optional[DataFile]:
        abs_p = pathlib.Path(path).resolve()
        try:
            abs_p.relative_to(self.root_dir)  # throws if not inside root
        except ValueError:
            return None  # outside the root
        f = DataFile(self.root_dir, abs_p)
        return f

    def clean(self, file: pathlib.Path = None):
        orig_files = []
        for f in self.root_dir.iterdir():
            if f.name != '.minion':
                self.collect_original_files(DataFile(self.root_dir, f), orig_files)

        derived_files = {}
        self.collect_metafiles(self.meta_root_dir, derived_files)

        rel_p = file.relative_to(self.root_dir)  # throws if not inside root
        if file and rel_p.name != '':
            clean_files = [rel_p] if rel_p in derived_files else []
        else:
            clean_files = list(filter(lambda f: f in derived_files, orig_files))

        for cf in clean_files:
            for mf in derived_files[cf]:
                self.__clean(mf, derived_files)

    def __clean(self, file_path: pathlib.Path, derived_files: Dict[pathlib.Path, List[pathlib.Path]]):
        orig_path = self.root_dir / file_path
        meta_path = pathlib.Path((self.meta_root_dir / file_path).as_posix() + ".MF")

        for dest in derived_files.get(file_path, []):
            self.__clean(dest, derived_files)
        # delete the 'real' file
        if orig_path.is_file():
            self.logger.debug(f"rm {orig_path.as_posix()}")
            orig_path.unlink()
        elif orig_path.is_dir() and len(list(orig_path.iterdir())) == 0:
            self.logger.debug(f"rmdir {orig_path.as_posix()}")
            orig_path.rmdir()
        elif orig_path.exists():
            self.logger.debug(f"leave {orig_path.as_posix()}")
        if meta_path.is_file():
            # delete the meta file and empty meta directories
            self.logger.debug(f"rm {meta_path.as_posix()}")
            meta_path.unlink()
            meta_parent = meta_path.parent
            while len(list(meta_parent.iterdir())) == 0 and not meta_parent.name.startswith('.'):
                self.logger.debug(f"rmdir {meta_parent.as_posix()}")
                meta_parent.rmdir()
                meta_parent = meta_parent.parent

    def collect_metafiles(self, meta_path: pathlib.Path, meta: Dict[pathlib.Path, List[pathlib.Path]]):
        if meta_path.is_dir():
            for m in sorted(meta_path.iterdir()):
                self.collect_metafiles(m, meta)
        elif meta_path.is_file():
            file_path = pathlib.Path((self.root_dir / meta_path).relative_to(self.meta_root_dir).as_posix()[:-3])
            with meta_path.open("r") as f:
                js = json.load(f)
            source = js.get('source')
            if source:
                full_src = pathlib.Path(source)
                ml = meta.get(full_src, [])
                self.__collect_derived_files(full_src, file_path, ml)
                ml.append(file_path)
                self.logger.debug(f"derived {full_src.as_posix()} -> {file_path.as_posix()}")
                meta[full_src] = ml

    def __collect_derived_files(self, source: pathlib.Path, file: pathlib.Path, files: List[pathlib.Path]):
        path = self.root_dir / file
        if path.is_dir():
            for f in path.iterdir():
                # assume all files without own metadata are part of the same 'result'
                f_meta = self.get_metadata(self.get_file_by_path(f.as_posix()))
                if not f_meta:
                    rel_f = f.relative_to(self.root_dir)
                    self.__collect_derived_files(source, rel_f, files)
                    self.logger.debug(f"derived {source.as_posix()} -> {rel_f.as_posix()}")
                    files.append(rel_f)

    def collect_original_files(self, file: DataFile, originals: List[pathlib.Path]):
        if file.is_dir():
            for f in file.list_files():
                self.collect_original_files(f, originals)
        if file.is_file():
            meta_f = self.get_metadata(file, parent_meta=True)
            if not meta_f:
                self.logger.debug(f"original {file.relative_path().as_posix()}")
        originals.append(file.relative_path())

    def apply_rule(self, in_file: DataFile, out_file: DataFile, rule: Rule, target_callback=None) -> DataFile:
        if not out_file.path.parent.exists():
            # parent directory missing - create it and metafile for it
            # - we kind of assume only single level of directories created for values (bad)
            out_file.path.parent.mkdir(parents=True)
            parent_m = FileMetadata(out_file.parent(), in_file)  # no rule
            parent_m_f = self.get_meta_path(out_file.path.parent)
            parent_m_f.parent.mkdir(exist_ok=True, parents=True)
            with parent_m_f.open("w") as f:
                json.dump(parent_m.to_json(), f)

        m = FileMetadata(out_file, in_file, rule)
        rule.apply(in_file, out_file, m)
        if m.exit_code !=0:
            m.print_output()
            raise Exception(f"Rule '{rule.name}' exit code: {m.exit_code}")

        meta_f = self.get_meta_path(out_file.path)
        meta_f.parent.mkdir(exist_ok=True, parents=True)
        with meta_f.open("w") as f:
            json.dump(m.to_json(), f)

        if target_callback:
            target_callback(m)
        return out_file

    def resolve_rules(self, file: DataFile) -> Dict[str, Rule]:
        live_rules: Dict[str, Rule] = {}
        self.__resolve_rules(file, live_rules)
        return live_rules

    def __resolve_rules(self, file: DataFile, live_rules: Dict[str, Rule]) -> Optional[RuleFile]:
        meta = self.get_metadata(file, parent_meta=True)
        if not meta:
            # no metafile, either original or metafile deleted
            rule_file = self.rules.get_rule_file()  # default rules
        elif meta.source is None or meta.rule is None:
            # no source (should always have?)
            rule_file = self.rules.get_rule_file()  # default rules
        else:
            source_rule_file = self.__resolve_rules(meta.source, live_rules)
            if not source_rule_file:
                live_rules.clear()
                return None
            rule_file = self.rules.get_rule_file().get_sub_rules(meta.rule)
            if not rule_file:
                live_rules.clear()
                return None
            if '*' not in rule_file.extend:
                rule_names = set(live_rules.keys()).copy()
                for n in filter(lambda r: r not in rule_file.extend, rule_names):
                    del live_rules[n]
        if rule_file:
            live_rules.update(rule_file.rule_map)
        return rule_file

    def get_meta_path(self, path: pathlib.Path) -> pathlib.Path:
        rel_p = path.relative_to(self.root_dir)  # throws if not inside root
        meta_f = pathlib.Path((self.meta_root_dir / rel_p).as_posix() + ".MF")
        return meta_f

    def get_metadata(self, file: DataFile, parent_meta: bool = False) -> Optional[FileMetadata]:
        meta_f = self.get_meta_path(file.path)
        if not meta_f.exists():
            if parent_meta and not file.is_root():
                return self.get_metadata(file.parent(), parent_meta)
            return None
        with meta_f.open("r") as f:
            js = json.load(f)
        m = self._read_file_metadata(file, js)
        return m


class SubDirectoryFileManager(FileManager):
    def __init__(self, registry: FileRegistry):
        super(SubDirectoryFileManager, self).__init__(registry)

    def apply_rule(self, file: DataFile, rule: Rule, target_callback=None) -> DataFile:
        t_path = self.__target_for(file, rule.name)
        target = DataFile(self.registry.root_dir, t_path)
        out_file = self.registry.apply_rule(file, target, rule, target_callback)
        return out_file

    def get_target(self, file: DataFile, rule_name: str) -> Optional[DataFile]:
        t_path = self.__target_for(file, rule_name)
        target = DataFile(self.registry.root_dir, t_path)
        if target.exists():
            return target
        return None

    def get_all_targets(self, file: DataFile) -> Iterable[DataFile]:
        t_path = self.__target_for(file)
        return [DataFile(self.registry.root_dir, f) for f in t_path.iterdir()]

    def get_data_for(self, path: pathlib.Path) -> pathlib.Path:
        if path.suffix == '.d':
            return path.parent / path.name[:-2]
        else:
            return path

    @classmethod
    def __target_for(cls, source: DataFile, rule: str = None) -> pathlib.Path:
        a_dir = pathlib.Path(source.path.as_posix() + '.d')
        t = a_dir / rule if rule else a_dir
        return t
