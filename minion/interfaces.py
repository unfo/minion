import datetime
import logging
import pathlib
import sys
from typing import Iterable, Optional, List, Any, Dict, Set

from minion import condition


class DataFile:
    def __init__(self, root: pathlib.Path, path: pathlib.Path):
        self.root = root
        self.path = path

    def relative_path(self) -> pathlib.Path:
        return self.path.relative_to(self.root)

    def name(self) -> str:
        return self.path.name

    def exists(self) -> bool:
        return self.path.exists()

    def is_root(self) -> bool:
        return self.path == self.root

    def is_file(self) -> bool:
        return self.path.is_file()

    def is_dir(self) -> bool:
        return self.path.is_dir()

    def read_string(self, value_default: Optional[str] = None, dir_default: Optional[str] = '<dir>') -> Optional[str]:
        if self.is_dir():
            return dir_default
        if not self.is_file():
            return value_default
        with self.path.open("r") as f:
            value = f.read()
        return value

    def read_bytes(self) -> Optional[bytes]:
        if self.is_dir():
            return None
        if not self.is_file():
            return None
        with self.path.open("rb") as f:
            value = f.read()
        return value

    def parent(self) -> Optional['DataFile']:
        if self.is_root():
            return None  # cannot go toward root any more
        else:
            return DataFile(self.root, self.path.parent)

    def list_files(self, recurse=False) -> Iterable['DataFile']:
        file_list = []
        for f in self.path.iterdir():
            if self.is_root() and f.name == '.minion':
                continue  # do not visit .dirs in the root
            df = DataFile(self.root, f)
            if recurse and f.is_dir():
                f_list = df.list_files(recurse=recurse)
                if not f_list:
                    f_list = [df]
                file_list.append(f_list)
            else:
                file_list.append(df)
        return file_list

    def __repr__(self) -> str:
        return self.relative_path().as_posix()

    def __eq__(self, other) -> bool:
        return isinstance(other, DataFile) and self.path == other.path

    def __hash__(self):
        return self.path.__hash__()

class Rule:
    def __init__(self, name: str):
        self.name = name
        self.predicate: Optional[condition.RuleCondition] = None

    def apply(self, in_file: DataFile, out_file: DataFile, meta: 'FileMetadata'):
        raise NotImplementedError

    def __repr__(self) -> str:
        return self.name


class RuleFile:
    def __init__(self, rules: List[Rule]):
        self.rules = rules
        self.rule_map: Dict[str, Rule] = {}
        self.parent: Optional['RuleFile'] = None
        self.sub_files: Dict[str, 'RuleFile'] = {}
        self.file_name = "unnamed"
        self.extend: Set[str] = set()
        self.update_maps()

    def get_rule(self, rule_name: str) -> Optional[Rule]:
        rule = self.rule_map.get(rule_name)
        if not rule and self.parent and ('*' in self.extend or rule_name in self.extend):
            rule = self.parent.get_rule(rule_name)
        return rule

    def get_sub_rules(self, rule: Rule) -> Optional['RuleFile']:
        return self.sub_files.get(rule.name)

    def update_maps(self):
        for r in self.rules:
            self.rule_map[r.name] = r


JSON_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'

class FileMetadata:
    def __init__(self, file: DataFile, source: Optional[DataFile] = None, rule: Optional[Rule] = None):
        self.file = file
        self.source = source
        self.rule = rule
        self.exit_code = 0
        self.log: List[(str, str)] = []
        self.timestamp = datetime.datetime.now()

    def to_json(self) -> Dict[str, Any]:
        js = {}
        if self.source:
            js['source'] = self.source.relative_path().as_posix()
        if self.rule:
            r_name = self.rule.name
            js['rule'] = r_name
        js['timestamp'] = self.timestamp.strftime(JSON_TIME_FORMAT)
        if self.exit_code:
            js['exit_code'] = self.exit_code
        if self.log:
            js['log'] = self.log
        return js

    def print_output(self):
        for t, line in self.log:
            if t == 'err':
                sys.stderr.write(line)
                sys.stderr.write('\n')
            else:
                print(line)


class RuleLibrary:
    def get_rule_file(self) -> Optional[RuleFile]:
        raise NotImplementedError

    def resolve(self, rule_name: str) -> Optional[Rule]:
        return self.get_rule_file().get_rule(rule_name)

    def list_rules(self) -> List[Rule]:
        return self.get_rule_file().rules


class FileRegistry:
    def __init__(self, rules: RuleLibrary):
        self.rules = rules
        self.logger = logging.getLogger('registry')

    def get_file_by_path(self, path: str) -> Optional[DataFile]:
        return None

    def apply_rule(self, in_file: DataFile, out_file: DataFile, rule: Rule, target_callback=None) -> DataFile:
        raise NotImplementedError

    def resolve_rules(self, file: DataFile) -> Dict[str, Rule]:
        raise NotImplementedError

    def clean(self):
        pass

    def get_metadata(self, file: DataFile, parent_meta: bool = False) -> Optional[FileMetadata]:
        return None

    def _read_file_metadata(self, file: DataFile, json: Dict[str, Any]) -> FileMetadata:
        m = FileMetadata(file)
        src = json.get('source')
        if src:
            m.source = self.get_file_by_path((file.root / src).as_posix())
            if not m.source:
                raise Exception(f"Could not resolve source {src}")
            r_rules = self.resolve_rules(m.source)
            m.rule = r_rules.get(json.get('rule')) if 'rule' in json else None
        m.log = json.get('log', [])
        return m


class FileManager:
    def __init__(self, registry: FileRegistry):
        self.registry = registry
        self.rules = self.registry.rules

    def apply_rule(self, file: DataFile, rule: Rule, target_callback) -> DataFile:
        raise NotImplementedError

    def get_target(self, file: DataFile, name: str) -> Optional[DataFile]:
        return None

    def get_all_targets(self, file: DataFile) -> Iterable[DataFile]:
        return []

    def get_data_for(self, path: pathlib.Path) -> pathlib.Path:
        return path
