import pathlib
import re

from typing import Optional, Dict, Any


def unquote(value: str, require_quotes: bool = False) -> Optional[str]:
    if not value:
        return value
    if value.startswith('"') and value.endswith('"'):
        return value[1:-1]
    if value.startswith("'") and value.endswith("'"):
        return value[1:-1]
    if require_quotes:
        return None
    return value


class ConditionValues:
    def get(self, value_name: str, default_value: str = None) -> Optional[str]:
        return None


class RuleCondition:

    def evaluate(self, values: ConditionValues) -> str:
        return ""


class ConditionReference(RuleCondition):
    def __init__(self, reference: str):
        self.reference = reference
        self.quoted = unquote(reference, require_quotes=True)

    def evaluate(self, values: ConditionValues) -> str:
        if self.quoted is not None:
            # Fixed value
            return self.quoted
        # attribute
        v = values.get(self.reference)
        v = v or ''  # avoid retuning None
        return v

    def __str__(self) -> str:
        return f'{self.reference}'


class ConditionSimpleMatch(RuleCondition):
    def __init__(self, value: RuleCondition, pattern: RuleCondition):
        self.value = value
        self.pattern = pattern

    def evaluate(self, values: ConditionValues) -> str:
        v = self.value.evaluate(values)
        pat_v = self.pattern.evaluate(values)
        if not pat_v or not pat_v.strip():
            return pat_v
        # split = list(filter(lambda s: s, pat_v.split("*")))
        split = pat_v.split("*")
        i = 0
        off = 0
        len_v = len(v)
        s = split[0]
        len_s = len(s)
        if len_s > 0:
            if len_v < i + len_s or v[i:i + len_s] != s:
                return ''
            off += len_s
            i += 1
        while i < len(split):
            s = split[i]
            len_s = len(s)
            if len_s > 0:
                off = v.find(s, off)
                if off < 0:
                    return ''
            i += 1
            off += len_s
        if split[-1] == '' or off == len_v:
            return "True"
        return ''

    def __str__(self) -> str:
        return f'{self.value} // {self.pattern}'


class EqualCondition(RuleCondition):
    def __init__(self, left: RuleCondition, right: RuleCondition):
        self.left = left
        self.right = right

    def evaluate(self, values: ConditionValues) -> str:
        lv = self.left.evaluate(values)
        rv = self.right.evaluate(values)
        if lv == rv:
            return "True"
        return ""

    def __str__(self) -> str:
        return f'{self.left} == {self.right}'


class AndCondition(RuleCondition):
    def __init__(self, left: RuleCondition, right: RuleCondition):
        self.left = left
        self.right = right

    def evaluate(self, values: ConditionValues) -> str:
        lv = self.left.evaluate(values)
        if not lv:
            return lv
        rv = self.right.evaluate(values)
        return rv

    def __str__(self) -> str:
        return f"({self.left} and {self.right})"


class OrCondition(RuleCondition):
    def __init__(self, left: RuleCondition, right: RuleCondition):
        self.left = left
        self.right = right

    def evaluate(self, values: ConditionValues) -> str:
        lv = self.left.evaluate(values)
        if lv:
            return lv
        rv = self.right.evaluate(values)
        return rv

    def __str__(self) -> str:
        return f"{self.left} or {self.right}"


class NotCondition(RuleCondition):
    def __init__(self, sub: RuleCondition):
        self.sub = sub

    def evaluate(self, values: ConditionValues) -> str:
        v = self.sub.evaluate(values)
        if not v:
            return "True"
        return ""

    def __str__(self) -> str:
        return f"not {self.sub}"
