import pathlib
from typing import Dict, List, Optional

from minion.rules import Rule
from minion.interfaces import RuleFile, RuleLibrary
from minion import file_lexer


class FileBasedRuleLibrary(RuleLibrary):
    def __init__(self, root_dir: pathlib.Path):
        files = self.__parse_rules(root_dir)
        if 'default' not in files:
            raise Exception("Missing 'default.rules'")
        self.root = files.pop('default')
        self.root.sub_files = files

    def __parse_rules(self, dir: pathlib.Path) -> Dict[str, RuleFile]:
        files: Dict[str, RuleFile] = {}
        for file in dir.iterdir():
            if file.is_file() and file.suffix == '.rules':
                with file.open("r") as f:
                    rule = file_lexer.parse(f.read())
                rule.file_name = file.name
                files[file.name[:-6]] = rule
        return files

    def get_rule_file(self) -> Optional[RuleFile]:
        return self.root
