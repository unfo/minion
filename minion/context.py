import json
import logging
import pathlib
from io import TextIOBase, IOBase, TextIOWrapper
from typing import List, Optional, Tuple

from minion.interfaces import Rule
from minion.persistence import DefaultFileRegistry, SubDirectoryFileManager, DataFile
from minion.rule_manager import RuleManager
from minion.rule_library import FileBasedRuleLibrary


class MinionContext:
    def __init__(self, focus: pathlib.Path):
        self.focus_dir = focus.resolve()
        self.logger = logging.getLogger('minion')
        self.files = self.scan_root_directory(self.focus_dir)
        self.registry = self.files.registry
        self.rules = self.files.rules
        self.root_dir = self.files.registry.root_dir

    def scan_root_directory(self, focus: pathlib.Path) -> RuleManager:
        files = None
        for d in [focus] + list(focus.parents):
            if self._is_root_dir(d):
                # go for the most _rootish_ directory, to ignore fake roots in analyzed files
                self.logger.debug(f"root at {d.as_posix()}")
                rules = FileBasedRuleLibrary(d / ".minion")
                files = RuleManager(SubDirectoryFileManager(DefaultFileRegistry(d, rules=rules)))
        if not files:
            raise Exception(f"Could not locate .minion under {focus.as_posix()}")
        return files

    def clean(self, path: pathlib.Path):
        return self.files.files.registry.clean(path)

    def go_to(self, path: pathlib.Path) -> DataFile:
        file: Optional[DataFile] = None
        path_list = self._reverse_path(path)
        for p in path_list:
            data_p = self.files.files.get_data_for(p)
            if data_p.exists():
                file = DataFile(self.root_dir, data_p)
            else:
                file = self.files.get_value(file, p.name)
                if not file:
                    break
        if not file or not file.exists():
            raise Exception(f"No such file {path.as_posix()}")
        return file

    def get_value(self, path: pathlib.Path, name: Optional[str] = None) -> DataFile:
        file = self.go_to(path)
        if name:
            file = self.files.get_value(file, name)
            if not file:
                raise Exception(f"No rule '{name}' to process {path.as_posix()}")
        return file

    def list_rules(self, path: pathlib.Path) -> List[Tuple[bool, Rule]]:
        file = self.go_to(path)
        rules = self.files.registry.resolve_rules(file)
        ret = []
        for r in rules.values():
            check = self.files.test_rule(file, r)
            ret.append((check, r))
        return ret

    def build(self, path: pathlib.Path) -> 'MinionContext':
        file = self.go_to(path)
        self._build(file)
        return self

    def _build(self, file: DataFile):
        if file.is_dir():
            # Note: we know there are no directory-input rules, so just visiting...
            for f in file.list_files():
                self._build(f)
            return

        rules = self.files.registry.resolve_rules(file)
        for r in rules.values():
            target = self.files.files.get_target(file, r.name)
            if target:
                # rule already applied
                self.logger.debug(f"+ {file}//{r.name} ")
                self._build(target)
                continue
            check = self.files.test_rule(file, r)
            if check:
                # apply the rule
                self.logger.debug(f"! {file}//{r.name}")
                target = self.files.files.apply_rule(file, r)
                self._build(target)
                continue
            # rule not applicable
            self.logger.debug(f"- {file}//{r.name}")

        if len(rules) == 0 and file.is_dir():
            # data directory, enter it
            for f in file.list_files():
                self._build(f)

    def show_file_hierarchy(self, writer: TextIOBase, path: pathlib.Path = None):
        root = self.registry.get_file_by_path(path) if path else DataFile(self.root_dir, self.root_dir)
        if not root.is_dir():
            return
        for f in root.list_files():
            if not f.path.name == '.minion':
                self.__show_hierarchy(writer, '', f)

    def __show_hierarchy(self, writer: TextIOBase, indent: str, file: DataFile):
        writer.write(f"{indent}{file.name()}")
        mf = self.files.registry.get_metadata(file)
        if mf:
            mf_source = mf.source.relative_path().as_posix() if mf.source else '?'
            mf_rule = mf.rule.name + ' ' if mf.rule else ''
            writer.write(f" <- {mf_rule}{mf_source}")
        writer.write("\n")
        if indent:
            indent = indent[:-2] + '  |-'
        else:
            indent += '  |-'
        if file.is_dir():
            for f in sorted(file.list_files(), key=lambda v: v.name()):
                self.__show_hierarchy(writer, indent, f)

    def explain(self, writer: TextIOBase, path: pathlib.Path) -> bool:
        file = self.files.registry.get_file_by_path(path)
        meta_f = self.files.registry.get_metadata(file, parent_meta=True)
        printed = False
        if meta_f and meta_f.source:
            if meta_f.source:
                src_file = pathlib.Path(self.root_dir / meta_f.source.path)
                printed = self.explain(writer, src_file)
            if meta_f.rule:
                if printed:
                    writer.write("\n")
                writer.write(f"# {meta_f.rule.name} {meta_f.timestamp}\n")
                writer.write(f"mkdir -p {meta_f.file.parent().relative_path().as_posix()}\n")
                printed = True
            for line in meta_f.log:
                if line[0] == 'in':
                    writer.write(f"{line[1]}\n")
                else:
                    writer.write(f"# {line[1]}\n")
                printed = True
        return printed

    def _reverse_path(self, path: pathlib.Path) -> List[pathlib.Path]:
        path_list = ([path] + list(path.parents))
        path_list.reverse()
        for i, p in enumerate(path_list):
            if p == self.root_dir:
                return path_list[i:]
        raise Exception(f"The given path not inside minion directory: {path.as_posix()}")

    @classmethod
    def _is_root_dir(cls, dir: pathlib.Path):
        return (dir / ".minion").is_dir()