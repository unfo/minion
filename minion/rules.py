import os
import shutil
import subprocess
import re
import sys
from typing import List

from minion.interfaces import DataFile, Rule, FileMetadata


class CommandRule(Rule):
    def __init__(self):
        super(CommandRule, self).__init__('unnamed')
        self.commands: List[str] = []
        self.pattern = re.compile('(\\$[a-zA-Z_@<][a-zA-Z0-9_]*)|(\\$\\([^()$]+\\))')

    def apply(self, in_file: DataFile, out_file: DataFile, meta: FileMetadata):
        work_dir = in_file.root.resolve()
        in_path = in_file.relative_path()
        out_path = out_file.relative_path()

        new_env = os.environ.copy()
        new_env['IN'] = in_path.as_posix()
        new_env['OUT'] = out_path.as_posix()

        replace = {
            '$IN': in_path.as_posix(),
            '$OUT': out_path.as_posix(),
            '$(IN)': in_path.as_posix(),
            '$(OUT)': out_path.as_posix(),
            '$<': in_path.as_posix(),
            '$@': out_path.as_posix(),
        }

        exit_code = 0
        for c in self.commands:
            c_str = self.pattern.split(c)
            c_str = filter(lambda s: s, c_str)
            c_str = [replace.get(x, x) for x in c_str]
            command = ''.join(c_str)
            meta.log.append(('in', command))
            print(command)
            completed = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, cwd=work_dir.as_posix(), env=new_env)
            exit_code = completed.returncode
            if completed.stdout:
                sys.stdout.buffer.write(completed.stdout)
                txt = completed.stdout.decode('ascii', errors='ignore').strip()
                for line in txt.split("\n"):
                    meta.log.append(('out', line))
            if completed.stderr:
                sys.stderr.buffer.write(completed.stderr)
                txt = completed.stderr.decode('ascii', errors='ignore').strip()
                for line in txt.split("\n"):
                    meta.log.append(('err', line))
            if exit_code != 0:
                break
        meta.exit_code = exit_code


