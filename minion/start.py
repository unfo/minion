import argparse
import logging
import pathlib
import sys
from typing import List

from minion import context


def main(args: List[str] = sys.argv[1:]):
    m_parser = argparse.ArgumentParser()
    m_parser.add_argument("-l", "--log", dest="log_level", choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                          help="Set the logging level", default='INFO')
    subparsers = m_parser.add_subparsers(dest='sub_command', )

    get_parser = subparsers.add_parser('get', help='Get path [rule] value')
    get_parser.add_argument('path', help='The path')
    get_parser.add_argument('name', nargs='?', help='Optional rule')

    ls_parser = subparsers.add_parser('ls', help='List files and sources')
    ls_parser.add_argument('path', default='.', nargs='?', help='The path')

    rules_parser = subparsers.add_parser('rules', help='List active rules (* marks ones that check)')
    rules_parser.add_argument('path', default='.', nargs='?', help='The path')

    build_parser = subparsers.add_parser('build', help='Build all rules recursively')
    build_parser.add_argument('path', default='.', nargs='?', help='The path')

    clean_parser = subparsers.add_parser('clean', help='Clean all files created by rules')
    clean_parser.add_argument('path', default='.', nargs='?', help='The path')

    explain_parser = subparsers.add_parser('explain', help='Explain how a file is created by rules')
    explain_parser.add_argument('path', default='.', nargs='?', help='The path')

    subparsers.add_parser('help')

    args = m_parser.parse_args(args)
    logging.basicConfig(format='%(message)s', level=getattr(logging, args.log_level))

    if args.log_level not in {'DEBUG'}:
        sys.tracebacklimit = 0

    sub_command = args.sub_command
    if not sub_command or sub_command == 'help':
        m_parser.print_help()
        return

    path = pathlib.Path(args.path).resolve()
    ctx = context.MinionContext(path)
    if sub_command == 'get':
        value_name = args.name
        value = ctx.get_value(path, value_name)
        if value.is_dir():
            for f in value.list_files(recurse=True):
                print(f.path.relative_to(value.path).as_posix())
        else:
            raw = value.read_bytes()
            if raw is not None:
                sys.stdout.buffer.write(raw)
    elif sub_command == 'ls':
        ctx.show_file_hierarchy(sys.stdout, path)
    elif sub_command == 'rules':
        for check, r in ctx.list_rules(path):
            if check:
                print(f"{r.name}*")
            else:
                print(f"{r.name}")
    elif sub_command == 'build':
        ctx.build(path)
    elif sub_command == 'clean':
        ctx.clean(path)
    elif sub_command == 'explain':
        ctx.explain(sys.stdout, path)
    else:
        raise Exception(f"Unknown sub command: {args.sub_command}")
