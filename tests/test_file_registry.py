import pathlib

from minion.context import MinionContext
from minion.persistence import DefaultFileRegistry
from . import test_context

establish_directory = test_context.establish_directory


def test_create_metadata():
    root_dir = establish_directory('context', rules='copy', sources='txt_2')
    ctx = MinionContext(root_dir)
    data = ctx.get_value(root_dir / 'b.txt', name='copy')
    assert data.read_string() == 'This is text B\n'
    data = ctx.get_value(root_dir / 'a.txt', name='copy')
    assert data.read_string() == 'This is text A\n'

    meta_a = ctx.registry.get_metadata(ctx.registry.get_file_by_path(root_dir / 'a.txt.d' / 'copy'))
    assert meta_a.source == ctx.registry.get_file_by_path(root_dir / 'a.txt')
    assert meta_a.rule == ctx.rules.resolve('copy')
    assert meta_a.log == [['in', 'cp -a a.txt a.txt.d/copy']]
    meta_b = ctx.registry.get_metadata(ctx.registry.get_file_by_path(root_dir / 'b.txt.d' / 'copy'))
    assert meta_b.source == ctx.registry.get_file_by_path(root_dir / 'b.txt')
    assert meta_b.rule == ctx.rules.resolve('copy')
    assert meta_b.log == [['in', 'cp -a b.txt b.txt.d/copy']]
