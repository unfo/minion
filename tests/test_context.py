import io
import os
import pathlib
import shutil
from typing import List

from minion.context import MinionContext

SAMPLES = 'tests/samples'
WORK_DIR = os.getcwd()


def establish_directory(name: str, rules: str, sources: str) -> pathlib.Path:
    sample_rules_dir = pathlib.Path(__file__).parent / "rules" / rules
    sample_sources_dir = pathlib.Path(__file__).parent / "samples" / sources

    root_dir = pathlib.Path(__file__).parent / ("_" + name)
    shutil.rmtree(root_dir, ignore_errors=True)
    print(f"Test directory {root_dir.as_posix()}")
    minion_dir = root_dir / '.minion'
    minion_dir.mkdir(parents=True)
    for r_file in sample_rules_dir.iterdir():
        shutil.copy(r_file, minion_dir)
    for s_file in sample_sources_dir.iterdir():
        if s_file.is_dir():
            shutil.copytree(s_file, root_dir / s_file.name)
        else:
            shutil.copy(s_file, root_dir / s_file.name)
    return root_dir


def test_rules():
    root_dir = establish_directory('context', rules='copy', sources='txt_2')
    ctx = MinionContext(root_dir)
    assert ctx.list_rules(root_dir / 'a.txt') == [(False, ctx.rules.resolve('copy'))]


def test_nested_rules():
    root_dir = establish_directory('context', rules='strings_and_zip', sources='zip_txt')
    ctx = MinionContext(root_dir)
    assert ctx.list_rules(root_dir / 'txt.zip') == [(True, ctx.rules.resolve('strings')),
                                                    (True, ctx.rules.resolve('zip'))]
    assert ctx.list_rules(root_dir / 'txt.zip.d' / 'strings') == []
    assert ctx.list_rules(root_dir / 'txt.zip.d' / 'zip' / 'b.txt') == \
           [(True, ctx.rules.resolve('strings')), (False, ctx.rules.resolve('zip'))]
    assert ctx.list_rules(root_dir / 'txt.zip.d' / 'zip' / 'b.txt.d' / 'strings') == []


def test_build():
    root_dir = establish_directory('context', rules='threesome', sources='zip_txt')
    ctx = MinionContext(root_dir).build(root_dir)

    assert ctx.registry.get_file_by_path(root_dir / 'txt.zip.d' / 'type').read_string() == 'application/zip\n'
    assert ctx.registry.get_file_by_path(root_dir / 'txt.zip.d' / 'strings').read_string() is not None
    assert ctx.registry.get_file_by_path(root_dir / 'txt.zip.d' / 'zip' / 'a.txt').read_string() == "This is text A\n"
    assert ctx.registry.get_file_by_path(root_dir / 'txt.zip.d' / 'zip' / 'a.txt.d' / 'type').read_string() \
        == "text/plain\n"
    assert ctx.registry.get_file_by_path(root_dir / 'txt.zip.d' / 'zip' / 'a.txt.d' / 'strings').read_string() is None
    assert ctx.registry.get_file_by_path(root_dir / 'txt.zip.d' / 'zip' / 'b.txt').read_string() == "This is text B\n"
    assert ctx.registry.get_file_by_path(root_dir / 'txt.zip.d' / 'zip' / 'b.txt.d' / 'type').read_string() \
        == "text/plain\n"
    assert ctx.registry.get_file_by_path(root_dir / 'txt.zip.d' / 'zip' / 'b.txt.d' / 'strings').read_string() is None

    buf = io.StringIO()
    ctx.show_file_hierarchy(buf)
    value = buf.getvalue()
    assert value == """txt.zip
txt.zip.d <- txt.zip
  |-strings <- strings txt.zip
  |-type <- type txt.zip
  |-zip <- zip txt.zip
    |-a.txt
    |-a.txt.d <- txt.zip.d/zip/a.txt
      |-type <- type txt.zip.d/zip/a.txt
    |-b.txt
    |-b.txt.d <- txt.zip.d/zip/b.txt
      |-type <- type txt.zip.d/zip/b.txt
"""


def test_clean():
    root_dir = establish_directory('context', rules='threesome', sources='zip_txt')
    ctx = MinionContext(root_dir).build(root_dir)

    ctx.clean(root_dir)
    buf = io.StringIO()
    ctx.show_file_hierarchy(buf)
    value = buf.getvalue()
    assert value == "txt.zip\n"


def test_explain():
    root_dir = establish_directory('context', rules='threesome', sources='zip_txt')
    ctx = MinionContext(root_dir).build(root_dir)

    buf = io.StringIO()
    ctx.explain(buf, root_dir / 'txt.zip.d' / 'zip' / 'a.txt')
    value = buf.getvalue()
    # timestamp in first line and some random white space in output
    assert "\n".join([s.strip() for s in value.split("\n")[1:]]) == """mkdir -p txt.zip.d
unzip txt.zip -d txt.zip.d/zip
# Archive:  txt.zip
#  extracting: txt.zip.d/zip/a.txt
#  extracting: txt.zip.d/zip/b.txt
"""
