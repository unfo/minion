import pathlib

from minion.condition import ConditionReference, ConditionSimpleMatch, ConditionValues


def test_values():
    values = ConditionValues()
    c = ConditionReference("nosuch")
    r = c.evaluate(values)
    assert r is not None
    assert not r


def test_matcher():
    values = ConditionValues()
    
    c = ConditionSimpleMatch(ConditionReference('"Title.docx"'), ConditionReference('"*.docx"'))
    r = c.evaluate(values)
    assert r

    c = ConditionSimpleMatch(ConditionReference('"jeeabbajuu"'), ConditionReference('"*a*a*"'))
    r = c.evaluate(values)
    assert r

    c = ConditionSimpleMatch(ConditionReference('"jeeabbxjuu"'), ConditionReference('"*a*a*"'))
    r = c.evaluate(values)
    assert not r

    c = ConditionSimpleMatch(ConditionReference('"jeeabbxjuu"'), ConditionReference('"jeeabbxjuu"'))
    r = c.evaluate(values)
    assert r

    c = ConditionSimpleMatch(ConditionReference('"jeeabbxjuu"'), ConditionReference('"*bb*juu"'))
    r = c.evaluate(values)
    assert r

    c = ConditionSimpleMatch(ConditionReference('"jeeabbxjuu"'), ConditionReference('"*bbG*juu"'))
    r = c.evaluate(values)
    assert not r

    c = ConditionSimpleMatch(ConditionReference('"jeeabbxjuu"'), ConditionReference('"*bbxjuu"'))
    r = c.evaluate(values)
    assert r

    c = ConditionSimpleMatch(ConditionReference('"jeeabbxjuu"'), ConditionReference('"*jeeabbxjuu"'))
    r = c.evaluate(values)
    assert r

    c = ConditionSimpleMatch(ConditionReference('"jeeabbxjuu"'), ConditionReference('"jeeabbxjuu*"'))
    r = c.evaluate(values)
    assert r

    c = ConditionSimpleMatch(ConditionReference('"jeeabbxjuu"'), ConditionReference('"jeeab*"'))
    r = c.evaluate(values)
    assert r

    c = ConditionSimpleMatch(ConditionReference('"jeeabbxjuu"'), ConditionReference('"jeeab*bxjuu"'))
    r = c.evaluate(values)
    assert r

    c = ConditionSimpleMatch(ConditionReference('"jeeabbxjuu"'), ConditionReference('"eeabbxjuu"'))
    r = c.evaluate(values)
    assert not r
