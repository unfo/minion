# Minion

:warning: Work in progress... currently best to go away and come back later!

Minion is a tool to build (semi)-automated analysis systems using your favorite command-line
tools and *minion rules*.

There are some samples under `samples/` in the git project.

## File structure

Root of the minion directory hierarchy is marked with `.minion` file, e.g.

    some-directory/
        .minion/
          default.rules
          specific.rules
          meta/
        file-a
        directory-b/
          file-c

In the example minion command operates for data files under `some-directory`,
which is indicated by the `.minion` directory. All other files and directories,
and respective sub-files and sub-directories, can be processed by minion.

Minion rule files have suffix `.rules` and they are placed into `.minion`
directory. There can be other files and directories under `.minion`, which
you should generally not use directly, but rather through the `minion` command.

## Minion rules

A minion *rule* takes a *source file* and produces a *target*. The target
can be a file or a directory with files and sub-directories under it.
Minion rules are in one or several files with suffix `.rules` in the `.minion`
directory. The rules from `default.rules` file are the default rules.

When *building*, the default rules are applied to derive targets from the
original source files,
but the targets themselves are no longer processed by the default rules.
E.g. if a rule `zip` unpacks target files from a zip-file,
those files are not processed further as default.
However, if there is a rule-file matching name of the rule
(e.g. `zip.rules` for `zip` rule), then these rules are applied for
targets produced by the rule.
(As a spoiler it can be told that a single line rule file `keep all`
keep all rules to be applied to targets)

A minion rule has the general format:

    rule <name> [when <condition>]
        <command-1>
        <command-2>
        ...
        <command-n>

Inside the commands variables `$IN` and `$OUT` are replaced by the source file
and target file names. When required, you can also use variables `$(IN)` and `$(OUT)`

## Minion sub commands

The minion sub-commands are the following

| Sub command             | Description             |
|-------------------------|-------------------------|
| build [path]            | Build all target |
| ls [path]               | List files and applies rules and sources |
| get [path] [rule]       | Get value of a rule in given path |
| rules [path]            | List available rules, mark with * the ones which check |
| explain [path]          | Explain how we generated a file |
| clean [path]            | Clean all targets |
