(require 'generic-x)
(define-generic-mode 
  'minion-rules-mode                ;; name of the mode to create
  '("#")                            ;; comments start with #
  '("rule" "set" "keep"
    "$IN" "when" "$OUT" "batch")           ;; some keywords
  '(("=" . 'font-lock-operator)     ;; '=' etc are operators
    ("and" . 'font-lock-operator)
    ("or" . 'font-lock-operator)
    ("!=" . 'font-lock-operator)
    ("==" . 'font-lock-operator)
    ("//" . 'font-lock-operator)    
    (";" . 'font-lock-builtin))     ;; ';' is a a built-in 
  '("\\.rules$")                    ;; files for which to activate this mode 
  nil                               ;; other functions to call
  "A mode for editing CinCan Minion rule files"            ;; doc string for this mode
)
