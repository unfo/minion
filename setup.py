from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='minion',
    version='0.0.1',
    author="Rauli Kaksonen",
    author_email="rauli.kaksonen@gmail.com",
    description='Minion orchestration tool',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/cincan/minion",
    packages=['minion'],
    install_requires=['lark-parser'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points = {
        'console_scripts': ['minion=minion.start:main'],
    },
    python_requires='>=3.6',
)
